
import './App.css';

import {Container, Form} from 'react-bootstrap';
import { useState, useEffect } from 'react';
// importing AppNavBar function from the AppNavBar.js
import AppNavBar from './components/AppNavBar.js';
/*import Banner from './Banner.js;'
import Highlights from './Highlights.js';*/
import Home from './pages/Home.js';
import Courses from './pages/Courses.js'
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import NotFound from './pages/NotFound.js';
import CourseView from './components/CourseView.js';

// import modules from react-router-dom for the routing
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'

// import the UserProvider
import { UserProvider } from './UserContext.js';


function App() {

	const [user, setUser] = useState(null);

	useEffect(() => {
		console.log(user)
	}, [user]);

	const unSetUser = () => {
		localStorage.clear();
	}

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)

			if (localStorage.getItem('token') !== null) {	
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			} else {
				setUser(null);
			}
		})
		
	}, [])

	// Storing information in a context object is done by providing the information using the corresponding "Provider" and passing the information thru the prop value;
	// all information/data provided to the Provider component can be accessed later on from the context object properties
	
  return (
	<UserProvider value={ {user, setUser, unSetUser} }>
		<Router>
			<AppNavBar/>
			<Routes>
				<Route path='/' element = {<Home/>}/>
				<Route path='/courses' element = {<Courses/>}/>
				<Route path='/login' element = {<Login/>}/>
				<Route path='/register' element = {<Register/>}/>
				<Route path='/logout' element = {<Logout/>}/>
				<Route path='/course/:courseId' element = {<CourseView/>}/>
				<Route path='*' element = {<NotFound/>}/>
			</Routes>
		</Router>
	</UserProvider>
  );
}

export default App;