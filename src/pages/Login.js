import { Fragment, useEffect, useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2'


export default function Login () {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);
    // const [user, setUser] = useState(localStorage.getItem("email"));

    // Allows us to consume the UserContext object and it's properties for user validation

    const {user, setUser} = useContext(UserContext);

    const navigate = useNavigate();

    console.log(user);

    function login(event){
        event.preventDefault();

        // if you want to add the email of the authenticated user in the local storage
            // Syntax:
                // localStorage.setItem("propertyName", value)
        

            /* Process a fetch request to corresponding backend API */
            // Syntax: fetch('url', {options});

        fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password,
            })
        })
        .then(result => result.json())
        .then(data => {
            
            if (data === false) {
                Swal.fire({
                    title: "Authentication Failed!",
                    icon: 'error',
                    text: "Please try again!"
                })
            } else {
                localStorage.setItem('token', data.auth)
                retrieveUserDetails(localStorage.getItem('token'));

                Swal.fire({
                    title: "Authentication Successful!",
                    icon: 'success',
                    text: "Welcome to Zuitt"
                })
                
                navigate('/');
            }

            
        })

        const retrieveUserDetails = (token) => {

            // the token sent as part of the request's header information

            fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(result => result.json())
            .then(data => {
                console.log(data)

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
        }
                
                
        // localStorage.setItem("email", email);

        // setUser(localStorage.getItem("email"));

        // alert("You are now logged in.");
        // setEmail('');
        // setPassword('');

        // navigate("/")
    }

    useEffect(() => {
        if (email !== "" && password !== "") {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    })

    return (
        user ?
        <Navigate to = "*"/>
        :
        <Fragment>

            <h2 className='mt-5'>Login</h2>
        
            <Form className='mt-4' onSubmit = {event => login(event)}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value = {email}
                        onChange = {event => setEmail(event.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>
        
                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value = {password}
                        onChange = {event => setPassword(event.target.value)}
                        required
                />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Keep me logged in" />
                </Form.Group>

                {
                    isActive ? 
                    <Button variant="primary" type="submit">
                        Login
                    </Button>
                    :
                    <Button variant="danger" type="submit" disabled>
                        Login
                    </Button>
                }
            </Form>
        </Fragment>
    );

}